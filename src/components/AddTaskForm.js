import React from "react";

const AddTaskForm = props =>{
    return(
        <div className="add-task">
            <input type="text" placeholder="I need to do.." className="add-inp"/>
            <button className="add-btn">Add</button>
        </div>
    )
}
export default AddTaskForm;