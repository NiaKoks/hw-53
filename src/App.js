import React, { Component } from 'react';
import './App.css';
import AddTaskForm from './components/AddTaskForm.js';
class App extends Component {
  state:{
    task:[text,id]
  }
  render() {
    return (
        <div className="add-task">
            <button onClick={this.clickHandler} className="add-btn">Add</button>
            <input type="text" className="add-inp"/>
        </div>
    );
  }
}

export default App;
